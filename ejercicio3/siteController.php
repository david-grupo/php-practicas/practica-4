
<?php

function actionFormulario(){
   return[
       "vista"=>"formulario.php"
       ]; 
}

function actionMedia(){
   $datos=$_REQUEST['numeros'];
   /*comprobamos si es array o string para saber su origen */
      if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
      $resultado=0;
   foreach($datos as $v){
    $resultado+=$v;   
   }
   $resultado/=count($datos);
   /*antes de mandar a la vista los numeros los convertimos a string*/
   $datos=implode("-",$datos);
   return[
       'vista' => 'resultado.php',
       'mensaje' => 'La media calculada es :',
       'resultado' => $resultado,
       'numeros' => $datos,
   ]; 
}


function actionModa(){
    $datos=$_REQUEST['numeros'];
        if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
   $resultado=0;
   
   $cuenta = array_count_values($datos);
    arsort($cuenta);
    $resultado= key($cuenta);
    $datos=implode("-",$datos);
   return[
       'vista' => 'resultado.php',
       'mensaje' => 'La moda calculada es :',
       'resultado' => $resultado,
       'numeros' => $datos,
   ];
   
}

function actionMediana(){
    $datos=$_REQUEST['numeros'];
    if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
    $resultado=0;
    asort($datos);
    
    $resultado=$datos[2];
    $datos=implode("-",$datos);
    
    return[
        'vista' => 'resultado.php',
       'mensaje' => 'La mediana calculada es :',
       'resultado' => $resultado,
       'numeros' => $datos,
    ];
}

function actionDesviacion(){
    $datos=$_REQUEST['numeros'];
    if(gettype($datos)=="string"){
            $datos= explode("-", $_REQUEST['numeros']);    
        }
    $resultado=0;$suma=0;$total=0;$totalb=0;$totalc=0;
    foreach($datos as $v){
        $suma=$suma+$v;
    }
    $primero=$suma/count($datos);
    foreach($datos as $v){
        $total=$total+(($v-$primero)*($v-$primero));
        
        
    }
    $totalb=$total/count($datos);
    $totalc=sqrt($totalb);
    $resultado=$totalc;
    $datos=implode("-",$datos);
    return[
        'vista' => 'resultado.php',
       'mensaje' => 'La desviacion calculada es :',
       'resultado' => $resultado,
       'numeros' => $datos,
    ];
}



          if (isset($_REQUEST["boton"])) {
              switch($_REQUEST['boton']){
                  case 'media':
                      $datos=actionMedia();
                      break;
                  case 'moda':
                      $datos=actionModa();
                      break;
                  case 'mediana':
                      $datos=actionMediana();
                      break;
                  case 'desviacion':
                      $datos=actionDesviacion();
                      break;
                  default:
                      $datos=actionFormulario;
                  
                  }
                  
          }else{
              $datos=actionFormulario();
              
          }
           
        
          